#NiceFish-SpringMybatis

这是NiceFish的Java版后台。

纯前端的NiceFish在这里：http://git.oschina.net/mumu-osc/NiceFish

#用法

这是一个MyEclipse2014工程，最简单的SpringMVC+MyBatis，所有依赖的jar包都提交上来了，不依赖任何其它内容。


- 克隆本项目的代码
- 用MyEclipse2014或更高版本import项目，就像这样：

![视频教程截图](docs/imgs/1.png)

- 然后你应该懂怎么做了

修改user表结构，请重写导入数据库,sql文件为nicefish_new.sql。