package com.nicefish.dao;

import com.nicefish.model.Permission;
import com.nicefish.util.base.BaseMapper;

public interface PermissionMapper extends BaseMapper<Permission, String>{
    
	
}