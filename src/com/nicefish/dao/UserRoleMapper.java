package com.nicefish.dao;

import com.nicefish.model.UserRole;
import com.nicefish.util.base.BaseMapper;

public interface UserRoleMapper extends BaseMapper<UserRole, String>{
    
	
}