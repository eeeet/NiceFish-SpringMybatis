package com.nicefish.dao;

import com.nicefish.model.Role;
import com.nicefish.util.base.BaseMapper;

public interface RoleMapper extends BaseMapper<Role, String>{
   
	
}