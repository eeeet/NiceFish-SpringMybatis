package com.nicefish.dao;

import com.nicefish.model.Upload;
import com.nicefish.util.base.BaseMapper;

public interface UploadMapper extends BaseMapper<Upload, String>{
    
	
}